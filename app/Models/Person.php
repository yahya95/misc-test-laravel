<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Services\Traits\UserStamps;
use App\Services\Traits\UuidGenerator;

/**
 * @property string $id
 * @property string $gender_id
 * @property string $birthplace_id
 * @property string $millat_id
 * @property string $holati_oilavi_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 * @property string $firstname
 * @property string $lastname
 * @property string $middlename
 * @property string $birthdate
 * @property string $address
 * @property string $address_temp
 * @property string $email
 * @property string $avatar
 * @property boolean $available
 * @property District $district
 * @property Gender $gender
 * @property HolatiOilavi $holatiOilavi
 * @property Millat $millat
 */
class Person extends Model
{


    use UuidGenerator {
        UuidGenerator::boot as boot_uuid;
    }
    use UserStamps {
        UserStamps::boot as boot_userstamp;
    }

    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'persons';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['gender_id', 'birthplace_id', 'millat_id', 'holati_oilavi_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'firstname', 'lastname', 'middlename', 'birthdate', 'address', 'address_temp', 'email', 'avatar', 'available'];

    public static function boot()
    {
        self::boot_uuid();
        self::boot_userstamp();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function district()
    {
        return $this->belongsTo('App\Models\Helpers\District', 'birthplace_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gender()
    {
        return $this->belongsTo('App\Models\Helpers\Gender');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function holatiOilavi()
    {
        return $this->belongsTo('App\Models\Helpers\HolatiOilavi');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function millat()
    {
        return $this->belongsTo('App\Models\Helpers\Millat');
    }
}
