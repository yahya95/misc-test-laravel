<?php

namespace App\Models\Helpers;

use Illuminate\Database\Eloquent\Model;

use App\Services\Traits\UserStamps;
use App\Services\Traits\UuidGenerator;


/**
 * @property string $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 * @property string $name
 * @property boolean $available
 * @property Person[] $persons
 */
class Millat extends Model
{

    use UuidGenerator {
        UuidGenerator::boot as boot_uuid;
    }
    use UserStamps {
        UserStamps::boot as boot_userstamp;
    }

    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'millat';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['created_at', 'updated_at', 'created_by', 'updated_by', 'name', 'available'];

    public static function boot()
    {
        self::boot_uuid();
        self::boot_userstamp();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function persons()
    {
        return $this->hasMany('App\Models\Person');
    }
}
