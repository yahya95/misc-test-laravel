<?php

namespace App\Models\Helpers;

use Illuminate\Database\Eloquent\Model;

use App\Services\Traits\UserStamps;
use App\Services\Traits\UuidGenerator;

/**
 * @property string $id
 * @property string $province_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 * @property string $name
 * @property boolean $available
 * @property Province $province
 * @property Person[] $persons
 */
class District extends Model
{

    use UuidGenerator {
        UuidGenerator::boot as boot_uuid;
    }
    use UserStamps {
        UserStamps::boot as boot_userstamp;
    }


    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['province_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'name', 'available'];


    public static function boot()
    {
        self::boot_uuid();
        self::boot_userstamp();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province()
    {
        return $this->belongsTo('App\Models\Helpers\Province');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function persons()
    {
        return $this->hasMany('App\Models\Person', 'birthplace_id');
    }


}
