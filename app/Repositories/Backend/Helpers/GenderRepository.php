<?php
/**
 * Created by PhpStorm.
 * User: K_Hayoev
 * Date: 03.08.2018
 * Time: 15:07
 */
namespace App\Repositories\Backend\Helpers;

use App\Models\Helpers\Gender;

class GenderRepository{
    private $model;

    public function __construct(Gender $model){
        $this->model = $model;
    }

    public function findByid($id){
        return $this->model->findOrFail($id);
    }

    public function findAll(){
        return $this->model->all();
    }

    public function create($data){
        return $this->model->create($data);
    }

    public function update($data,$id){
        return $this->model->find($id)->update($data);
    }
}