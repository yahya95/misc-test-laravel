<?php
/**
 * Created by PhpStorm.
 * User: K_Hayoev
 * Date: 03.08.2018
 * Time: 15:08
 */

namespace App\Repositories\Backend\Helpers;

use App\Models\Helpers\HolatiOilavi;

class HolatiOilaviRepository{
    private $model;

    public function __construct(HolatiOilavi $model){
        $this->model = $model;
    }

    public function findByid($id){
        return $this->model->findOrFail($id);
    }

    public function findAll(){
        return $this->model->all();
    }

    public function create($data){
        return $this->model->create($data);
    }

    public function update($data,$id){
        return $this->model->find($id)->update($data);
    }
}