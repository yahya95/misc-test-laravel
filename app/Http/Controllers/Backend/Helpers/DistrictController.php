<?php

namespace App\Http\Controllers\Backend\Helpers;

use App\Http\Requests\Backend\Helpers\District\StoreDistrictRequest;
use App\Http\Requests\Backend\Helpers\District\UpdateDistrictRequest;
use App\Http\Resources\Backend\Helpers\DistrictResource;
use App\Repositories\Backend\Helpers\DistrictRepository;
use App\Http\Controllers\Controller;

class DistrictController extends Controller
{

    private $districtRepository;

    function __construct(DistrictRepository $repository)
    {
        $this->districtRepository = $repository;
    }

    /**
     * Display a listing of the districts (nohiya).
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DistrictResource::collection($this->districtRepository->findAll());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Create a new district (nohiya)
     *
     * @param  \App\Http\Requests\Backend\Helpers\District\StoreDistrictRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDistrictRequest $request)
    {
        return $this->districtRepository->create($request->validated());
    }

    /**
     * Display the specified district (nohiya).
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return DistrictResource::make($this->districtRepository->findById($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified district (nohiya).
     *
     * @param  \App\Http\Requests\Backend\Helpers\District\UpdateDistrictRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDistrictRequest $request, $id)
    {
        $this->districtRepository->update($request->validated(), $id);
        return response('successfully updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
