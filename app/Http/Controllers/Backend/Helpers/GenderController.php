<?php

namespace App\Http\Controllers\Backend\Helpers;

use App\Http\Requests\Backend\Helpers\Gender\StoreGenderRequest;
use App\Http\Requests\Backend\Helpers\Gender\UpdateGenderRequest;
use App\Http\Resources\Backend\Helpers\GenderResource;
use App\Repositories\Backend\Helpers\GenderRepository;
use App\Http\Controllers\Controller;

class GenderController extends Controller
{

    private $genderRepository;

    function __construct(GenderRepository $repository)
    {
        $this->genderRepository = $repository;
    }


    /**
     * Display a listing of the gender.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return GenderResource::collection($this->genderRepository->findAll());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Create a new gender.
     *
     * @param  \App\Http\Requests\Backend\Helpers\Gender\StoreGenderRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGenderRequest $request)
    {
        return $this->genderRepository->create($request->validated());
    }

    /**
     * Display the specified gender.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return GenderResource::make($this->genderRepository->findById($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified gender.
     *
     * @param  \App\Http\Requests\Backend\Helpers\Gender\UpdateGenderRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGenderRequest $request, $id)
    {
        $this->genderRepository->update($request->validated(), $id);
        return response('successfully updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
