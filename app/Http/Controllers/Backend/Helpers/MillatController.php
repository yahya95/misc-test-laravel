<?php

namespace App\Http\Controllers\Backend\Helpers;

use App\Http\Requests\Backend\Helpers\Millat\StoreMillatRequest;
use App\Http\Requests\Backend\Helpers\Millat\UpdateMillatRequest;
use App\Http\Resources\Backend\Helpers\MillatResource;
use App\Repositories\Backend\Helpers\MillatRepository;
use App\Http\Controllers\Controller;

class MillatController extends Controller
{

    private $millatRepository;

    function __construct(MillatRepository $repository)
    {
        $this->millatRepository=$repository;
    }


    /**
     * Display a listing of the millat.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return MillatResource::collection($this->millatRepository->findAll());
    }

    /**
     * Show the form for creating a new millat.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Create a new millat.
     *
     * @param \App\Http\Requests\Backend\Helpers\Millat\StoreMillatRequest  $request
     * @return \App\Http\Resources\Backend\Helpers\MillatResource
     */
    public function store(StoreMillatRequest $request)
    {
        return $this->millatRepository->create($request->validated());
    }

    /**
     * Display the specified millat.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return MillatResource::make($this->millatRepository->findById($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified millat.
     *
     * @param \App\Http\Requests\Backend\Helpers\Millat\UpdateMillatRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMillatRequest $request, $id)
    {
        $this->millatRepository->update($request->validated(),$id);
        return response('successfully updated',200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
