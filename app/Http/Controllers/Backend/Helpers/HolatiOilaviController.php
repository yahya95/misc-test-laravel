<?php

namespace App\Http\Controllers\Backend\Helpers;

use App\Http\Requests\Backend\Helpers\HolatiOilavi\StoreHolatiOilaviRequest;
use App\Http\Requests\Backend\Helpers\HolatiOilavi\UpdateHolatiOilaviRequest;
use App\Http\Resources\Backend\Helpers\HolatiOilaviResource;
use App\Repositories\Backend\Helpers\HolatiOilaviRepository;
use App\Http\Controllers\Controller;

class HolatiOilaviController extends Controller
{

    private $holatRepository;

    function __construct(HolatiOilaviRepository $repository)
    {
        $this->holatRepository = $repository;
    }

    /**
     * Display a listing of the holati oilavi.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return HolatiOilaviResource::collection($this->holatRepository->findAll());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Create a new holati oilavi.
     *
     * @param  \App\Http\Requests\Backend\Helpers\HolatiOilavi\StoreHolatiOilaviRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreHolatiOilaviRequest $request)
    {
        return $this->holatRepository->create($request->validated());
    }

    /**
     * Display the specified holati oilavi.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return HolatiOilaviResource::make($this->holatRepository->findById($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified holati oilavi.
     *
     * @param  \App\Http\Requests\Backend\Helpers\HolatiOilavi\UpdateHolatiOilaviRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateHolatiOilaviRequest $request, $id)
    {
        $this->holatRepository->update($request->validated(), $id);
        return response('successfully updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
