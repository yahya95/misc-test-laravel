<?php

namespace App\Http\Controllers\Backend\Helpers;

use App\Http\Requests\Backend\Helpers\Province\StoreProvinceRequest;
use App\Http\Requests\Backend\Helpers\Province\UpdateProvinceRequest;
use App\Http\Resources\Backend\Helpers\ProvinceResource;
use App\Repositories\Backend\Helpers\ProvinceRepository;
use App\Http\Controllers\Controller;

class ProvinceController extends Controller
{
    private $provinceRepository;

    function __construct(ProvinceRepository $repository)
    {
        $this->provinceRepository = $repository;
    }

    /**
     * Display a listing of the province.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ProvinceResource::collection($this->provinceRepository->findAll());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Create a new province.
     *
     * @param  \App\Http\Requests\Backend\Helpers\Province\StoreProvinceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProvinceRequest $request)
    {
        return $this->provinceRepository->create($request->validated());
    }

    /**
     * Display the specified province.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return ProvinceResource::make($this->provinceRepository->findById($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified province.
     *
     * @param  \App\Http\Requests\Backend\Helpers\Province\UpdateProvinceRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProvinceRequest $request, $id)
    {
        $this->provinceRepository->update($request->validated(), $id);
        return response('successfully updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
