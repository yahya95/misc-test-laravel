<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\Person\StorePersonRequest;
use App\Http\Requests\Backend\Person\UpdatePersonRequest;
use App\Http\Resources\Backend\PersonResource;
use App\Repositories\Backend\Helpers\PersonRepository;
use App\Http\Controllers\Controller;

class PersonController extends Controller
{

    private $personRepository;

    function __construct(PersonRepository $repository)
    {
        $this->personRepository = $repository;
    }

    /**
     * Display a listing of the person.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PersonResource::collection($this->personRepository->findAll());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Create new person.
     *
     * @param  \App\Http\Requests\Backend\Person\StorePersonRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePersonRequest $request)
    {
        return $this->personRepository->create($request->validated());
    }

    /**
     * Display the specified person.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return PersonResource::make($this->personRepository->findById($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified person.
     *
     * @param  \App\Http\Requests\Backend\Person\UpdatePersonRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePersonRequest $request, $id)
    {
        $this->personRepository->update($request->validated(), $id);
        return response('successfully updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
