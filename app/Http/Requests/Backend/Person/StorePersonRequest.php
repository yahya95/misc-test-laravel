<?php

namespace App\Http\Requests\Backend\Person;

use Illuminate\Foundation\Http\FormRequest;

class StorePersonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'=>'required|max:100',
            'lastname'=>'required|max:100',
            'email' => 'required|email|max:255',
            'gender_id' => 'required|max:36',
            'birthdate' => 'required|date_format:Y-m-d|before:today',
            'avatar' => 'dimensions:min_width=100,min_height=200',
            'address' => 'required|min:10|max:255',
            'birthplace_id' => 'required|max:36',
            'millat_id' =>'required|max:36',
            //'holati_oilavi_id' =>'required|max:36',
        ];
    }
}
