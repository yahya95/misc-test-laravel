<?php
/**
 * Created by PhpStorm.
 * User: F_Abdurashidov
 * Date: 16.04.2018
 * Time: 13:50
 */

namespace App\Services\Traits;


use Illuminate\Support\Facades\Auth;



trait UserStamps
{
    public static function boot(){
        parent::boot();

        static::creating(function($model){
            if(Auth::user()){
                $model->created_by=Auth::user()->id;
            }
        });

        static::updating(function($model){
           if(Auth::user()){
               $model->updated_by=Auth::user()->id;
           }
        });
    }

}