<?php
namespace App\Services\Traits;

use Webpatser\Uuid\Uuid;

trait UuidGenerator
{

    /**
     * Boot function from laravel.
     */
    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Uuid::generate()->string;
        });
    }
}