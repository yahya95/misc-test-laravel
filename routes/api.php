<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=>'v1','namespace' => 'Backend'], function () {


    Route::post('login', 'Access\Auth\AuthController@login');
    Route::post('logout', 'Access\Auth\AuthController@logout');
    Route::post('refresh', 'Access\Auth\AuthController@refresh');
    Route::post('me', 'Access\Auth\AuthController@me');

    Route::group(['middleware' => ['jwt.auth']], function () {


        //All routes must be required in this group
        require(__DIR__ . '/Api/Backend/Helpers/Millat.php');
        require(__DIR__ . '/Api/Backend/Helpers/District.php');
        require(__DIR__ . '/Api/Backend/Helpers/Gender.php');
        require(__DIR__ . '/Api/Backend/Helpers/HolatiOilavi.php');
        require(__DIR__ . '/Api/Backend/Helpers/Province.php');
        require(__DIR__ . '/Api/Backend/Person.php');

    });

});
