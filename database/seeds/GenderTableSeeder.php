<?php

use Illuminate\Database\Seeder;

class GenderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gender')->insert([
            'id'=> Uuid::generate()->string,
            'name' => 'Мард',
            'created_by' =>'',
        ]);

        DB::table('gender')->insert([
            'id'=> Uuid::generate()->string,
            'name' => 'Зан',
            'created_by' =>'',
        ]);
    }
}
