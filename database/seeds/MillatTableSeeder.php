<?php

use Illuminate\Database\Seeder;

class MillatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('millat')->insert([
            'id'=> Uuid::generate()->string,
            'name' => 'Тоҷик',
            'created_by' =>'',
        ]);
    }
}
