<?php

use Illuminate\Database\Seeder;

class ProvinceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        /*$province = new \App\Models\Helpers\Province();
        $province->name = 'Суғд';
        $province->save();*/

        $provinceUuid = Uuid::generate()->string;

        DB::table('provinces')->insert([
            'id'=> $provinceUuid,
            'name' => 'Суғд',
            'created_by' => '',
        ]);

        DB::table('districts')->insert([
            'id'=> Uuid::generate()->string,
            'province_id'=> $provinceUuid,
            'name' => 'Айнӣ',
            'created_by' => '',
        ]);
    }
}
