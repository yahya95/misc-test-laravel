<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persons', function (Blueprint $table) {
            $table->uuid('id');
            $table->timestamps();
            $table->uuid('created_by');
            $table->uuid('updated_by')->nullable();
            $table->string('firstname',100);
            $table->string('lastname',100);
            $table->string('middlename',100)->nullable();
            $table->uuid('gender_id')->nullable();
            $table->foreign('gender_id')->references('id')->on('gender');
            $table->date('birthdate');
            $table->string('address',255);
            $table->string('address_temp',255)->nullable();;
            $table->uuid('birthplace_id');
            $table->foreign('birthplace_id')->references('id')->on('districts');
            $table->string('email',255)->nullable();
            $table->string('mobile_phone',40)->nullable();
            $table->string('home_phone',40)->nullable();
            $table->string('other_phone',40)->nullable();
            $table->string('avatar',255)->nullable();
            $table->uuid('millat_id');
            $table->foreign('millat_id')->references('id')->on('millat');
            $table->uuid('holati_oilavi_id')->nullable();
            $table->foreign('holati_oilavi_id')->references('id')->on('holati_oilavi');
            $table->boolean('available')->default(true);

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persons');
    }
}
